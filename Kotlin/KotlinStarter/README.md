# Kotlin Starter
---

This project provides a starter template for any Kotlin projects with Kotlin DSL (build.gradle.kts).


### Building Environment
---

This project is built based on the following environments:

- Android Gradle Plugin : 3.4.1
- Gradle : 5.1.1
- Kotlin : 1.3.31
- Compile SDK : 28
- Minimum SDK : 21
- Target SDK: 28


### Technologies
---

This application implements the following technologies :

- Kotlin
- Gradle Kotlin DSL
- Autocompleting and IDE friendly dependency management using the buildSrc folder.


### Libraries
---

- Android Support Library