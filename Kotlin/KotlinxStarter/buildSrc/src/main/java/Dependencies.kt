// 1
// Define an object (singleton in Kotlin) named Versions.
object Versions {
    // 2
    // Define a constant named kotlin and assign it a value
    // that is the version of Kotlin language plugin you are using.
    const val kotlin = "1.3.31"

    // Plugins
    const val androidGradlePlugin = "3.4.1"

    // Build Config
    const val compileSdkVersion = 28
    const val minSdkVersion = 21
    const val targetSdkVersion = 28

    // AndroidX Support Library
    const val support = "1.0.2"
    const val constraintLayout = "1.1.3"

    // Testing
    const val junit = "4.12"
    const val testRunner = "1.2.0"
    const val espresso = "3.2.0"
}

object Releases {
    // App version
    const val appVersionCode = 1
    const val appVersionName = "1.0.0"
}

object Plugins {
    const val androidApplication = "com.android.application"
    const val kotlin = "kotlin-android"
    const val kotlinx = "kotlin-android-extensions"
    const val kapt = "kotlin-kapt"
}

object Configs {
    const val applicationId = "dev.idigit.android.kotlinxstarter"
    const val androidJUnitRunner = "androidx.test.runner.AndroidJUnitRunner"
}

object Proguards {
    const val optimize = "proguard-android-optimize.txt"
    const val rules = "proguard-rules.pro"
}

// 3
// Define another object named Libs.
object Libraries {

    // Plugins
    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.androidGradlePlugin}"

    // 4
    // Define a constant named kotlinStdLib and assign it a value
    // that is the string for the Kotlin standard library dependency.
    // Note how the kotlinStdLib value can reference values from the Versions singleton.
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"

    // Core Kotlin Extensions
    const val coreKtx = "androidx.core:core-ktx:${Versions.support}"

    // AndroidX Support Library
    const val appCompat = "androidx.appcompat:appcompat:${Versions.support}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"

    // Testing
    const val junit = "junit:junit:${Versions.junit}"
    const val testRunner = "androidx.test:runner:${Versions.testRunner}"
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espresso}"
}