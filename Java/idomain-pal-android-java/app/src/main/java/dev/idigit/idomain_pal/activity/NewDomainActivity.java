package dev.idigit.idomain_pal.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.Map;

import dev.idigit.idomain_pal.R;
import dev.idigit.idomain_pal.model.Domain;
import dev.idigit.idomain_pal.util.DateConverter;
import dev.idigit.idomain_pal.util.TextFieldDatePicker;

public class NewDomainActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String TAG = NewDomainActivity.class.getSimpleName();

    private EditText mDomainNameField;
    private EditText mRegisteredDateField;
    private EditText mExpiredDateField;
    private EditText mProviderNameField;
    private EditText mServerIPField;
    private EditText mPrimaryDNSField;
    private EditText mSecondaryDNSField;

    private FirebaseFirestore db;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_domain);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        db = FirebaseFirestore.getInstance();

        mDomainNameField = findViewById(R.id.tv_new_domain_name);
        mRegisteredDateField = findViewById(R.id.tv_new_domain_registered);
        mExpiredDateField = findViewById(R.id.tv_new_domain_expired);
        mProviderNameField = findViewById(R.id.tv_new_domain_provider_name);
        mServerIPField = findViewById(R.id.tv_new_domain_server_ip);
        mPrimaryDNSField = findViewById(R.id.tv_new_domain_primary_dns);
        mSecondaryDNSField = findViewById(R.id.tv_new_domain_secondary_dns);

        // Buttons
        findViewById(R.id.btn_add_new_domain).setOnClickListener(this);

        // Set date from date picker
        TextFieldDatePicker setRegisteredDate = new TextFieldDatePicker(mRegisteredDateField, this);
        TextFieldDatePicker setExpiredDate = new TextFieldDatePicker(mExpiredDateField, this);

    }

    private boolean validateForm() {
        boolean result = true;

        String domainName = mDomainNameField.getText().toString();
        String registeredDate = mRegisteredDateField.getText().toString();
        String expiredDate = mExpiredDateField.getText().toString();
        String providerName = mProviderNameField.getText().toString();
        String serverIP = mServerIPField.getText().toString();
        String primaryDNS = mPrimaryDNSField.getText().toString();
        String secondaryDNS = mSecondaryDNSField.getText().toString();

        if (TextUtils.isEmpty(domainName)) {
            mDomainNameField.setError("Required");
            mDomainNameField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter domain name", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mDomainNameField.setError(null);
        }

        if (TextUtils.isEmpty(registeredDate)) {
            mRegisteredDateField.setError("Required");
            mRegisteredDateField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter registered date", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mRegisteredDateField.setError(null);
        }

        if (TextUtils.isEmpty(expiredDate)) {
            mExpiredDateField.setError("Required");
            mExpiredDateField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter expired date", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mExpiredDateField.setError(null);
        }

        if (TextUtils.isEmpty(providerName)) {
            mProviderNameField.setError("Required");
            mProviderNameField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter provider name", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mProviderNameField.setError(null);
        }

        if (TextUtils.isEmpty(serverIP)) {
            mServerIPField.setError("Required");
            mServerIPField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter server IP", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mServerIPField.setError(null);
        }

        if (TextUtils.isEmpty(primaryDNS)) {
            mPrimaryDNSField.setError("Required");
            mPrimaryDNSField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter primary DNS", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mPrimaryDNSField.setError(null);
        }

        if (TextUtils.isEmpty(secondaryDNS)) {
            mSecondaryDNSField.setError("Required");
            mSecondaryDNSField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter secondary DNS", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mSecondaryDNSField.setError(null);
        }

        return result;
    }

    private void addDomain() {
        Log.d(TAG, "addNewDomain");

        if (!validateForm()) {
            return;
        }

        String domainName = mDomainNameField.getText().toString();
        Date registeredDate = DateConverter.fromTimestamp(mRegisteredDateField.getText().toString());
        Date expiredDate = DateConverter.fromTimestamp(mExpiredDateField.getText().toString());
        String providerName = mProviderNameField.getText().toString();
        String serverIP = mServerIPField.getText().toString();
        String primaryDNS = mPrimaryDNSField.getText().toString();
        String secondaryDNS = mSecondaryDNSField.getText().toString();

        final String userId = currentUser.getUid();

        Domain domain = new Domain(domainName, registeredDate, expiredDate, providerName, serverIP, primaryDNS, secondaryDNS, userId);

        Map<String, Object> domainValue = domain.toMap();

        CollectionReference domainRef = db.collection("domains");

        domainRef.add(domainValue)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot Domain added");
                        Toast.makeText(NewDomainActivity.this, "Domain Added", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding Domain document", e);
                        Toast.makeText(NewDomainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        // Option 2:
        // Add a new document with a generated ID
//        db.collection("domains")
//                .document()
//                .set(domainValue)
//                .addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        Log.d(TAG, "DocumentSnapshot Domain added");
//                        Toast.makeText(NewDomainActivity.this, "Domain Added", Toast.LENGTH_LONG).show();
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.w(TAG, "Error adding Domain document", e);
//                        Toast.makeText(NewDomainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
//                    }
//                });
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_add_new_domain) {
            addDomain();
        }
    }
}
