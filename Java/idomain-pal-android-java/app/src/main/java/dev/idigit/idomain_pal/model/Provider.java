package dev.idigit.idomain_pal.model;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Provider POJO
 */
@IgnoreExtraProperties
public class Provider implements Serializable {
    @Exclude private String id;
    private String name;
    private String url;
    private String username;
    private String password;
    private String email;
    private String userId;

    public Provider() {

    }

    public Provider(String name, String url, String username, String password, String email, String userId) {
        this.name = name;
        this.url = url;
        this.username = username;
        this.password = password;
        this.email = email;
        this.userId = userId;
    }

    public Provider(String name) {
        this.name = name;
    }

    // [START provider_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("url", url);
        result.put("username", username);
        result.put("password", password);
        result.put("email", email);
        result.put("userId", userId);

        return result;
    }
    // [END provider_to_map]


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

//    @Override
//    public int getViewType() {
//        return DomainProvider.VIEW_TYPE_PROVIDER;
//    }
}
