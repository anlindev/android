package dev.idigit.idomain_pal.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

// Firebase Authentication
import com.google.firebase.auth.FirebaseAuth;

// Firebase Firestore

import com.google.firebase.auth.FirebaseUser;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;

import dev.idigit.idomain_pal.R;
import dev.idigit.idomain_pal.fragment.DomainListFragment;
import dev.idigit.idomain_pal.fragment.ProviderListFragment;
import dev.idigit.idomain_pal.adapter.TabAdapter;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public final static String TAG = MainActivity.class.getSimpleName();

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseAuth.AuthStateListener mAuthListner;

    private static final int ADD_ACTION_POSITION = 1;
    private SpeedDialView mSpeedDialView;

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private String[] tabTitles = {"Domain", "Provider"};
    private int[] tabIcons = {
            R.drawable.ic_language_black_24dp,
            R.drawable.ic_filter_drama_black_24dp
    };

    ImageView ivIcon;
    TextView mUsername, mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSpeedDialView = findViewById(R.id.speedDial);
//        speedDialView.addActionItem(
//                new SpeedDialActionItem.Builder(R.id.fab, R.drawable.ic_link_white_24dp)
//                        .create()
//        );

        mSpeedDialView.inflate(R.menu.menu_fab);

        mSpeedDialView.setOnActionSelectedListener(new SpeedDialView.OnActionSelectedListener() {
            @Override
            public boolean onActionSelected(SpeedDialActionItem speedDialActionItem) {
            switch (speedDialActionItem.getId()) {
                case R.id.action_add_domain:

                    // Toast.makeText(getApplicationContext(), "Add a provider", Toast.LENGTH_SHORT).show();
                    // return false; // true to keep the Speed Dial open

                    onClickAddDomain();

                    Toast.makeText(getApplicationContext(), "Add a domain", Toast.LENGTH_SHORT).show();

                    mSpeedDialView.close();
                    break; // true to keep the Speed Dial open
                case R.id.action_add_provider:

                    onClickAddProvider();

                    Toast.makeText(getApplicationContext(), "Add a provider", Toast.LENGTH_SHORT).show();

                    mSpeedDialView.close();
                    break; // true to keep the Speed Dial open

//                        mSpeedDialView.replaceActionItem(new SpeedDialActionItem.Builder(R.id.fab_remove_action,
//                            R.drawable.ic_delete_white_24dp)
//                            .setLabel(getString(R.string.label_remove_action))
//                            .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorGoogleButton,
//                                    getTheme()))
//                            .create(), ADD_ACTION_POSITION);

                default:
                    return false;
            }
            return true;
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        viewPager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tab_layout);

        adapter = new TabAdapter(getSupportFragmentManager(), this);
        adapter.addFragment(new DomainListFragment(), tabTitles[0], tabIcons[0]);
        adapter.addFragment(new ProviderListFragment(), tabTitles[1], tabIcons[1]);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        highLightCurrentTab(0); // for initial selected tab view

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                highLightCurrentTab(position); // for tab change
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        // Change Tab selection when swipe ViewPager
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        if (savedInstanceState == null) {
            navigationView.getMenu().performIdentifierAction(R.id.nav_domains, 0);
        }

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

//        mAuthListner = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                startActivity(new Intent(MainActivity.this, LoginActivity.class));
//            }
//        };

        View view = navigationView.getHeaderView(0);
        ivIcon = view.findViewById(R.id.img_user_avatar);
        mUsername = view.findViewById(R.id.tv_username);
        mEmail = view.findViewById(R.id.tv_email);

        mUsername.setText(currentUser.getDisplayName());
        mEmail.setText(currentUser.getEmail());
    }

    /**
     *
     * @param position
     */
    private void highLightCurrentTab (int position){

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(adapter.getTabView(i));
        }
        TabLayout.Tab tab = tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(adapter.getSelectedTabView(position));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_domains:
//                title = "My Domains";
                viewPager.setCurrentItem(0);
                break;

            case R.id.nav_providers:
//                title = "My Providers";
                viewPager.setCurrentItem(1);
                break;

            case R.id.nav_profile:
                Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
                break;

            case R.id.nav_change_password:

                onClickChangePassword();

                Toast.makeText(this, "Change Password", Toast.LENGTH_SHORT).show();
                break;

            case R.id.nav_sign_out:

                onClickSignOut();

                Toast.makeText(this, "Exit to Login", Toast.LENGTH_SHORT).show();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        item.setChecked(true);
//        getSupportActionBar().setTitle(title);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void onClickChangePassword() {
        Intent intent = new Intent(MainActivity.this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    private void onClickSignOut() {
        mAuth.signOut();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void onClickAddDomain() {
        Intent intent = new Intent(getApplicationContext(), NewDomainActivity.class);
        startActivity(intent);
    }

    private void onClickAddProvider() {
        Intent intent = new Intent(getApplicationContext(), NewProviderActivity.class);
        startActivity(intent);
    }
}
