package dev.idigit.idomain_pal.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.Date;
import java.util.Map;

import dev.idigit.idomain_pal.R;
import dev.idigit.idomain_pal.model.Domain;
import dev.idigit.idomain_pal.util.DateConverter;
import dev.idigit.idomain_pal.util.TextFieldDatePicker;

public class EditDomainActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String TAG = EditDomainActivity.class.getSimpleName();

    private EditText mDomainNameField;
    private EditText mRegisteredDateField;
    private EditText mExpiredDateField;
    private EditText mProviderNameField;
    private EditText mServerIPField;
    private EditText mPrimaryDNSField;
    private EditText mSecondaryDNSField;

    private Domain domain;

    private FirebaseFirestore db;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_domain);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        domain = (Domain) getIntent().getSerializableExtra("domain");

        db = FirebaseFirestore.getInstance();

        mDomainNameField = findViewById(R.id.tv_edit_domain_name);
        mRegisteredDateField = findViewById(R.id.tv_edit_domain_registered);
        mExpiredDateField = findViewById(R.id.tv_edit_domain_expired);
        mProviderNameField = findViewById(R.id.tv_edit_domain_provider_name);
        mServerIPField = findViewById(R.id.tv_edit_domain_server_ip);
        mPrimaryDNSField = findViewById(R.id.tv_edit_domain_primary_dns);
        mSecondaryDNSField = findViewById(R.id.tv_edit_domain_secondary_dns);

        mDomainNameField.setText(domain.getName());
        mRegisteredDateField.setText(DateConverter.dateToTimestamp(domain.getRegistered()));
        mExpiredDateField.setText(DateConverter.dateToTimestamp(domain.getExpired()));
        mProviderNameField.setText(domain.getProviderName());
        mServerIPField.setText(domain.getServerIP());
        mPrimaryDNSField.setText(domain.getPrimaryDNS());
        mSecondaryDNSField.setText(domain.getSecondaryDNS());

        // Buttons
        findViewById(R.id.btn_delete_domain).setOnClickListener(this);
        findViewById(R.id.btn_update_domain).setOnClickListener(this);

        // Set date from date picker
        TextFieldDatePicker setRegisteredDate = new TextFieldDatePicker(mRegisteredDateField, this);
        TextFieldDatePicker setExpiredDate = new TextFieldDatePicker(mExpiredDateField, this);

    }

    private boolean validateForm() {
        boolean result = true;

        String domainName = mDomainNameField.getText().toString();
        String registeredDate = mRegisteredDateField.getText().toString();
        String expiredDate = mExpiredDateField.getText().toString();
        String providerName = mProviderNameField.getText().toString();
        String serverIP = mServerIPField.getText().toString();
        String primaryDNS = mPrimaryDNSField.getText().toString();
        String secondaryDNS = mSecondaryDNSField.getText().toString();

        if (TextUtils.isEmpty(domainName)) {
            mDomainNameField.setError("Required");
            mDomainNameField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter domain name", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mDomainNameField.setError(null);
        }

        if (TextUtils.isEmpty(registeredDate)) {
            mRegisteredDateField.setError("Required");
            mRegisteredDateField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter registered date", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mRegisteredDateField.setError(null);
        }

        if (TextUtils.isEmpty(expiredDate)) {
            mExpiredDateField.setError("Required");
            mExpiredDateField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter expired date", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mExpiredDateField.setError(null);
        }

        if (TextUtils.isEmpty(providerName)) {
            mProviderNameField.setError("Required");
            mProviderNameField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter provider name", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mProviderNameField.setError(null);
        }

        if (TextUtils.isEmpty(serverIP)) {
            mServerIPField.setError("Required");
            mServerIPField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter server IP", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mServerIPField.setError(null);
        }

        if (TextUtils.isEmpty(primaryDNS)) {
            mPrimaryDNSField.setError("Required");
            mPrimaryDNSField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter primary DNS", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mPrimaryDNSField.setError(null);
        }

        if (TextUtils.isEmpty(secondaryDNS)) {
            mSecondaryDNSField.setError("Required");
            mSecondaryDNSField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter secondary DNS", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mSecondaryDNSField.setError(null);
        }

        return result;
    }

    private void updateDomain() {
        Log.d(TAG, "updateDomain");

        if (!validateForm()) {
            return;
        }

        String domainName = mDomainNameField.getText().toString();
        Date registeredDate = DateConverter.fromTimestamp(mRegisteredDateField.getText().toString());
        Date expiredDate = DateConverter.fromTimestamp(mExpiredDateField.getText().toString());
        String providerName = mProviderNameField.getText().toString();
        String serverIP = mServerIPField.getText().toString();
        String primaryDNS = mPrimaryDNSField.getText().toString();
        String secondaryDNS = mSecondaryDNSField.getText().toString();

        final String userId = currentUser.getUid();

        Domain currentDomain = new Domain(domainName, registeredDate, expiredDate, providerName, serverIP, primaryDNS, secondaryDNS, userId);

        Map<String, Object> domainValue = currentDomain.toMap();

        // Add a new document with a generated ID
        db.collection("domains")
                .document(domain.getId())
                .set(domainValue, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot Domain updated");
                        finish();
                        gotoMain();
                        Toast.makeText(EditDomainActivity.this, "Domain updated", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error updating Domain document", e);
                        Toast.makeText(EditDomainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void deleteDomain() {
        Log.d(TAG, "deleteDomain");
        db.collection("domains").document(domain.getId()).delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "DocumentSnapshot Domain updated");
                            finish();
                            gotoMain();
                            Toast.makeText(EditDomainActivity.this, "Domain deleted", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();

        if (i == R.id.btn_update_domain) {
            updateDomain();
        } else if (i == R.id.btn_delete_domain) {
            deleteDomain();
        }
    }

    private void gotoMain() {
        Intent intent = new Intent(EditDomainActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
