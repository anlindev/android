package dev.idigit.idomain_pal.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

import dev.idigit.idomain_pal.activity.LoginActivity;

public class UserAuthentication {
    static SharedPreferences sharedPreferences;

    public static String getAuthUser(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        String userId = sharedPreferences.getString(Constant.PREF_USER_ID, "");
        String username = sharedPreferences.getString(Constant.PREF_EMAIL, "");
        String password = sharedPreferences.getString(Constant.PREF_PASSWORD, "");
        String userpass = username + ":" + password;

        String authString = Base64.encodeToString(userpass.getBytes(), Base64.DEFAULT);

        return authString;
    }

    public static void logout(Context context) {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(Constant.PREF_USER_ID, "");
        editor.putString(Constant.PREF_EMAIL, "");
        editor.putString(Constant.PREF_PASSWORD, "");
        editor.commit();
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }
}
