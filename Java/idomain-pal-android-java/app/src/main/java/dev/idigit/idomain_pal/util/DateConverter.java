package dev.idigit.idomain_pal.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {
    public static final String TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private static DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

    public static Date fromTimestamp(String value) {
        if (value != null) {
            try {
                return dateFormat.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }

    public static String dateToTimestamp(Date value) {

        return value == null ? null : dateFormat.format(value);
    }
}
