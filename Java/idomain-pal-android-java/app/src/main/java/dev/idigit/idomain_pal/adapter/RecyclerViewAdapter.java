package dev.idigit.idomain_pal.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import dev.idigit.idomain_pal.R;
import dev.idigit.idomain_pal.activity.EditDomainActivity;
import dev.idigit.idomain_pal.activity.EditProviderActivity;
import dev.idigit.idomain_pal.model.Domain;
import dev.idigit.idomain_pal.model.Provider;
import dev.idigit.idomain_pal.util.DateConverter;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = RecyclerViewAdapter.class.getSimpleName();
    private static final int VIEW_TYPE_DOMAIN = 0;
    private static final int VIEW_TYPE_PROVIDER = 1;

    private ArrayList<Object> mItems;

    Context context;

    // Constructor
    public RecyclerViewAdapter(Activity context, ArrayList<Object> data) {
        this.context = context;
        mItems = data;
    }

    @Override
    public int getItemViewType(int position) {
        if (mItems.get(position) instanceof Domain) {
            return VIEW_TYPE_DOMAIN;
        } else if (mItems.get(position) instanceof Provider) {
            return VIEW_TYPE_PROVIDER;
        }
        return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;

        if (viewType == VIEW_TYPE_DOMAIN) {
            itemView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.list_item_domain, parent, false);
            return new DomainViewHolder(itemView);
        } else {
            itemView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.list_item_provider, parent, false);
            return new ProviderViewHolder(itemView);
        }
    }

    // Invoked by layout manager to replace the contents of the views
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        int viewType = viewHolder.getItemViewType();
        switch (viewType) {
            case VIEW_TYPE_DOMAIN:
                ((DomainViewHolder) viewHolder).bindView(position);
                break;
            case VIEW_TYPE_PROVIDER:
                ((ProviderViewHolder) viewHolder).bindView(position);
                break;
        }
    }

    class DomainViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView mDomainCardView;
        TextView mDomainNameField;
        TextView mDomainExpiredField;
        View mDomainTextContainer;

        public DomainViewHolder(View itemView) {
            super(itemView);

            mDomainCardView = itemView.findViewById(R.id.cv_domain);
            mDomainNameField = itemView.findViewById(R.id.tv_domain_item_name);
            mDomainExpiredField = itemView.findViewById(R.id.tv_domain_item_expired);
            mDomainTextContainer = itemView.findViewById(R.id.text_container_domain);

            itemView.setOnClickListener(this);
        }

        public void bindView(int position) {
            Domain domain = (Domain) mItems.get(position);

            mDomainNameField.setText(domain.getName());
            String expiredDate = "expired on " + DateConverter.dateToTimestamp(domain.getExpired());
            mDomainExpiredField.setText(expiredDate);
        }

        @Override
        public void onClick(View v) {
            Domain domain = (Domain) mItems.get(getAdapterPosition());
            Intent intent = new Intent(context, EditDomainActivity.class);
            intent.putExtra("domain", domain);
            context.startActivity(intent);
        }

    }

    class ProviderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView mProviderCardView;
        TextView mProviderNameField;
        View mProviderTextContainer;

        public ProviderViewHolder(View itemView) {
            super(itemView);

            mProviderCardView = itemView.findViewById(R.id.cv_provider);
            mProviderNameField = itemView.findViewById(R.id.tv_provider_item_name);
            mProviderTextContainer = itemView.findViewById(R.id.text_container_provider);

            itemView.setOnClickListener(this);
        }

        public void bindView(int position) {
            Provider provider = (Provider) mItems.get(position);

            mProviderNameField.setText(provider.getName());
        }

        @Override
        public void onClick(View v) {
            Provider provider = (Provider) mItems.get(getAdapterPosition());
            Intent intent = new Intent(context, EditProviderActivity.class);
            intent.putExtra("provider", provider);
            context.startActivity(intent);
        }
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }

}
