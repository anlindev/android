package dev.idigit.idomain_pal.model;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Domain POJO
 */
@IgnoreExtraProperties
public class Domain implements Serializable {

    @Exclude private String id;
    private String name;
    private Date registered;
    private Date expired;
    private String providerName;
    private String serverIP;
    private String primaryDNS;
    private String secondaryDNS;
    private String userId;

    public Domain() {

    }

    public Domain(String name, Date registered, Date expired, String providerName, String serverIP, String primaryDNS, String secondaryDNS, String userId) {
        this.name = name;
        this.registered = registered;
        this.expired = expired;
        this.providerName = providerName;
        this.serverIP = serverIP;
        this.primaryDNS = primaryDNS;
        this.secondaryDNS = secondaryDNS;
        this.userId = userId;
    }

    public Domain(String name) {
        this.name = name;
    }

    // [START domain_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("registered", registered);
        result.put("expired", expired);
        result.put("providerName", providerName);
        result.put("serverIP", serverIP);
        result.put("primaryDNS", primaryDNS);
        result.put("secondaryDNS", secondaryDNS);
        result.put("userId", userId);
//        result.put("providerId", providerId);

        return result;
    }
    // [END domain_to_map]


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }

    public Date getExpired() {
        return expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getPrimaryDNS() {
        return primaryDNS;
    }

    public void setPrimaryDNS(String primaryDNS) {
        this.primaryDNS = primaryDNS;
    }

    public String getSecondaryDNS() {
        return secondaryDNS;
    }

    public void setSecondaryDNS(String secondaryDNS) {
        this.secondaryDNS = secondaryDNS;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

//    public String getProviderId() {
//        return providerId;
//    }
//
//    public void setProviderId(String providerId) {
//        this.providerId = providerId;
//    }

//    @Override
//    public int getViewType() {
//        return DomainProvider.VIEW_TYPE_DOMAIN;
//    }
}
