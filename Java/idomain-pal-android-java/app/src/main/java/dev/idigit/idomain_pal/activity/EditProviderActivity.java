package dev.idigit.idomain_pal.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.Map;

import dev.idigit.idomain_pal.R;
import dev.idigit.idomain_pal.model.Provider;

public class EditProviderActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String TAG = EditProviderActivity.class.getSimpleName();

    private EditText mProviderNameField;
    private EditText mProviderURLField;
    private EditText mLoginUsernameField;
    private EditText mLoginPasswordField;
    private EditText mContactEmailField;

    private Provider provider;

    private FirebaseFirestore db;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_provider);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        provider = (Provider) getIntent().getSerializableExtra("provider");

        db = FirebaseFirestore.getInstance();

        mProviderNameField = findViewById(R.id.tv_edit_provider_name);
        mProviderURLField = findViewById(R.id.tv_edit_provider_url);
        mLoginUsernameField = findViewById(R.id.tv_edit_provider_username);
        mLoginPasswordField = findViewById(R.id.tv_edit_provider_password);
        mContactEmailField = findViewById(R.id.tv_edit_provider_email);

        mProviderNameField.setText(provider.getName());
        mProviderURLField.setText(provider.getUrl());
        mLoginUsernameField.setText(provider.getUsername());
        mLoginPasswordField.setText(provider.getPassword());
        mContactEmailField.setText(provider.getEmail());

        // Buttons
        findViewById(R.id.btn_delete_provider).setOnClickListener(this);
        findViewById(R.id.btn_update_provider).setOnClickListener(this);
    }

    private boolean validateForm() {
        boolean result = true;

        String providerName = mProviderNameField.getText().toString();
        String providerURL = mProviderURLField.getText().toString();
        String loginUsername = mLoginUsernameField.getText().toString();
        String loginPassword = mLoginPasswordField.getText().toString();
        String contactEmail = mContactEmailField.getText().toString();

        if (TextUtils.isEmpty(providerName)) {
            mProviderNameField.setError("Required");
            mProviderNameField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter provider name", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mProviderNameField.setError(null);
        }

        if (TextUtils.isEmpty(providerURL)) {
            mProviderURLField.setError("Required");
            mProviderURLField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter provider URL", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mProviderURLField.setError(null);
        }

        if (TextUtils.isEmpty(loginUsername)) {
            mLoginUsernameField.setError("Required");
            mLoginUsernameField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter login username", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mLoginUsernameField.setError(null);
        }

        if (TextUtils.isEmpty(loginPassword)) {
            mLoginPasswordField.setError("Required");
            mLoginPasswordField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter login password", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mLoginPasswordField.setError(null);
        }

        if (TextUtils.isEmpty(contactEmail)) {
            mContactEmailField.setError("Required");
            mContactEmailField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter contact email", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mContactEmailField.setError(null);
        }

        return result;
    }

    private void updateProvider() {
        Log.d(TAG, "updateProvider");

        if (!validateForm()) {
            return;
        }

        String providerName = mProviderNameField.getText().toString();
        String providerURL = mProviderURLField.getText().toString();
        String loginUsername = mLoginUsernameField.getText().toString();
        String loginPassword = mLoginPasswordField.getText().toString();
        String contactEmail = mContactEmailField.getText().toString();

        final String userId = currentUser.getUid();

        Provider currentProvider = new Provider(providerName, providerURL, loginUsername, loginPassword, contactEmail, userId);

        Map<String, Object> providerValue = currentProvider.toMap();

        // Add a new document with a generated ID
        db.collection("providers")
                .document(provider.getId())
                .set(providerValue, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot Provider updated");
                        finish();
                        gotoMain();
                        Toast.makeText(EditProviderActivity.this, "Provider updated", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error updating Provider document", e);
                        Toast.makeText(EditProviderActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void deleteProvider() {
        Log.d(TAG, "deleteProvider");
        db.collection("providers").document(provider.getId()).delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "DocumentSnapshot Provider deleted");
                            finish();
                            gotoMain();
                            Toast.makeText(EditProviderActivity.this, "Provider deleted", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();

        if (i == R.id.btn_update_provider) {
            updateProvider();
        } else if (i == R.id.btn_delete_provider) {
            deleteProvider();
        }
    }

    private void gotoMain() {
        Intent intent = new Intent(EditProviderActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
