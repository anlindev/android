package dev.idigit.idomain_pal.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import dev.idigit.idomain_pal.R;
import dev.idigit.idomain_pal.model.Provider;
import dev.idigit.idomain_pal.adapter.RecyclerViewAdapter;

public class ProviderListFragment extends Fragment {

    public final static String TAG = DomainListFragment.class.getSimpleName();

    private RecyclerViewAdapter mProvidersAdapter;
    private RecyclerView mProvidersRecyclerView;
    private ProgressBar mProgressBar;

    private FirebaseUser currentUser;
    private FirebaseFirestore db;

    private ArrayList<Object> providerList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_layout,container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProgressBar = view.findViewById(R.id.pb_progress_cycle);
        mProvidersRecyclerView = view.findViewById(R.id.rv_item_list);
        mProvidersRecyclerView.setLayoutManager(new LinearLayoutManager(ProviderListFragment.this.getActivity()));
        mProvidersRecyclerView.setHasFixedSize(true);

        providerList = new ArrayList<>();

        mProvidersAdapter = new RecyclerViewAdapter(ProviderListFragment.this.getActivity(), providerList);
        mProvidersRecyclerView.setAdapter(mProvidersAdapter);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseFirestore.getInstance();

        db.collection("providers").whereEqualTo("userId", currentUser.getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        mProgressBar.setVisibility(View.GONE);

                        if (!queryDocumentSnapshots.isEmpty()) {
                            List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();

                            for (DocumentSnapshot documentSnapshot : list) {
                                Provider provider = documentSnapshot.toObject(Provider.class);
                                provider.setId(documentSnapshot.getId());
                                providerList.add(provider);
                            }

                            mProvidersAdapter.notifyDataSetChanged();
                        }

                    }
                });
    }
}
