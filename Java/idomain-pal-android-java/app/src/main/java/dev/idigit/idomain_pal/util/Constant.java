package dev.idigit.idomain_pal.util;

public class Constant {

    public static final String PREF_USER_ID = "user_id";
    public static final String PREF_EMAIL = "email";
    public static final String PREF_PASSWORD = "password";
    public static final String EXTRA_USER_ID = "dev.idigit.idomain_pal.USER_ID";

}
