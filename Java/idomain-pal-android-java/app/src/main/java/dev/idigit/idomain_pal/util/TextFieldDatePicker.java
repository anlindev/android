package dev.idigit.idomain_pal.util;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

public class TextFieldDatePicker implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    private EditText editText;
    private Context context;
    private Calendar calendar;

    public TextFieldDatePicker(EditText editText, Context context) {
        this.editText = editText;
        this.context = context;
        this.editText.setOnClickListener(this);
        calendar = Calendar.getInstance();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        editText.setText(DateConverter.dateToTimestamp(calendar.getTime()));
    }

    @Override
    public void onClick(View v) {
        new DatePickerDialog(context, this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

}
