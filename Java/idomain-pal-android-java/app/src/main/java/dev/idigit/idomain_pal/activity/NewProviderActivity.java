package dev.idigit.idomain_pal.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

import dev.idigit.idomain_pal.R;
import dev.idigit.idomain_pal.model.Provider;

public class NewProviderActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String TAG = NewDomainActivity.class.getSimpleName();

    private EditText mProviderNameField;
    private EditText mProviderURLField;
    private EditText mLoginUsernameField;
    private EditText mLoginPasswordField;
    private EditText mContactEmailField;

    private FirebaseFirestore db;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_provider);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        db = FirebaseFirestore.getInstance();

        mProviderNameField = findViewById(R.id.tv_new_provider_name);
        mProviderURLField = findViewById(R.id.tv_new_provider_url);
        mLoginUsernameField = findViewById(R.id.tv_new_provider_username);
        mLoginPasswordField = findViewById(R.id.tv_new_provider_password);
        mContactEmailField = findViewById(R.id.tv_new_provider_email);

        // Buttons
        findViewById(R.id.btn_add_new_provider).setOnClickListener(this);
    }

    private boolean validateForm() {
        boolean result = true;

        String providerName = mProviderNameField.getText().toString();
        String providerURL = mProviderURLField.getText().toString();
        String loginUsername = mLoginUsernameField.getText().toString();
        String loginPassword = mLoginPasswordField.getText().toString();
        String contactEmail = mContactEmailField.getText().toString();

        if (TextUtils.isEmpty(providerName)) {
            mProviderNameField.setError("Required");
            mProviderNameField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter provider name", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mProviderNameField.setError(null);
        }

        if (TextUtils.isEmpty(providerURL)) {
            mProviderURLField.setError("Required");
            mProviderURLField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter provider URL", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mProviderURLField.setError(null);
        }

        if (TextUtils.isEmpty(loginUsername)) {
            mLoginUsernameField.setError("Required");
            mLoginUsernameField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter login username", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mLoginUsernameField.setError(null);
        }

        if (TextUtils.isEmpty(loginPassword)) {
            mLoginPasswordField.setError("Required");
            mLoginPasswordField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter login password", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mLoginPasswordField.setError(null);
        }

        if (TextUtils.isEmpty(contactEmail)) {
            mContactEmailField.setError("Required");
            mContactEmailField.requestFocus();
            Toast.makeText(getApplicationContext(), "Please enter contact email", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            mContactEmailField.setError(null);
        }

        return result;
    }

    private void addProvider() {
        Log.d(TAG, "addNewProvider");

        if (!validateForm()) {
            return;
        }

        String providerName = mProviderNameField.getText().toString();
        String providerURL = mProviderURLField.getText().toString();
        String loginUsername = mLoginUsernameField.getText().toString();
        String loginPassword = mLoginPasswordField.getText().toString();
        String contactEmail = mContactEmailField.getText().toString();

        final String userId = currentUser.getUid();

        Provider provider = new Provider(providerName, providerURL, loginUsername, loginPassword, contactEmail, userId);

        Map<String, Object> providerValue = provider.toMap();

        CollectionReference providerRef = db.collection("providers");

        providerRef.add(providerValue)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot Provider added");
                        Toast.makeText(NewProviderActivity.this, "Provider Added", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding Provider document", e);
                        Toast.makeText(NewProviderActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_add_new_provider) {
            addProvider();
        }
    }
}
